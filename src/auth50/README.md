# ThinkPHP Auth权限控制

#### 介绍
1. 基于ThinkPHP 5.0的Auth权限控制类修改而来
2. 新增支持默认权限
3. 优化了部分查询性能不高的情况
4. 扩展了用户组权限字段长度
5. 调整了部分字段名称

#### 版本支持
```
php > 5.6.0
topthink/framework > 5.1.0
```

#### 功能特性
1. 是对规则进行认证，不是对节点进行认证。用户可以把节点当作规则名称实现对节点进行认证。

```
$auth=new Auth();
$auth->check('规则名称','用户id')
```

2. 可以同时对多条规则进行认证，并设置多条规则的关系（or或者and）

```
$auth=new Auth();
$auth->check('规则1,规则2','用户id','and')
```

> 第三个参数为and时表示，用户需要同时具有规则1和规则2的权限。 当第三个参数为or时，表示用户值需要具备其中一个条件即可。默认为or
4. 一个用户可以属于多个用户组(think_auth_roles_access表 定义了用户所属用户组)。我们需要设置每个用户组拥有哪些规则(think_auth_roles 定义了用户组权限)

5. 支持规则表达式。
在think_auth_rules 表中定义一条规则时，如果type为1， condition字段就可以定义规则表达式。 如定义{score}>5  and {score}<100  表示用户的分数在5-100之间时这条规则才会通过。

#### 数据库结构

```
-- ----------------------------
-- think_auth_rules，规则表，
-- id:主键，name：规则唯一标识, title：规则中文名称 status 状态：为1正常，为0禁用，condition：规则表达式，为空表示存在就验证，不为空表示按照条件验证
-- ----------------------------
DROP TABLE IF EXISTS `think_auth_rules`;
CREATE TABLE `think_auth_rules` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL DEFAULT '',
  `title` varchar(20) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `condition` varchar(100) NOT NULL DEFAULT '', # 规则附件条件,满足附加条件的规则,才认为是有效的规则
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否默认权限',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规则节点表';
-- ----------------------------
-- think_auth_roles 用户组表，
-- id：主键， title:用户组中文名称， rules：用户组拥有的规则id， 多个规则","隔开，status 状态：为1正常，为0禁用
-- ----------------------------
DROP TABLE IF EXISTS `think_auth_roles`;
CREATE TABLE `think_auth_roles` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT '角色名称',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '角色状态',
  `rules` text NOT NULL COMMENT '权限节点ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';
-- ----------------------------
-- think_auth_roles_access 用户组明细表
-- uid:用户id，role_id：用户组id
-- ----------------------------
DROP TABLE IF EXISTS `think_auth_roles_access`;
CREATE TABLE `admins_roles_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `role_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_role_id` (`uid`,`role_id`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色映射表';
```
