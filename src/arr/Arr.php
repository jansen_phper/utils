<?php
// +----------------------------------------------------------------------
// | 在我们年轻的城市里，没有不可能的事！
// +----------------------------------------------------------------------
// | Copyright (c) 2020 http://utils All rights reserved.
// +----------------------------------------------------------------------
// | Author : Jansen <6206574@qq.com>
// +----------------------------------------------------------------------
namespace jansen\utils\arr;
class Arr{
    /**
     * 把数据集转换成Tree
     * @param array  $list  要转换的数据集
     * @param string $pk    主键字段
     * @param string $pid   父级字段
     * @param string $child 子级字段
     * @param string $root  根节点值
     * @return array
     * @author:Jansen <6206574@qq.com>
     */
    public static function toTree(array $list, string $pk='id', string $pid = 'pid', string $child = '_child', string $root = '0') {
        // 创建Tree
        $result = array();
        // 创建基于主键的数组引用
        $refer = array();
        foreach ($list as $key => $data) {
            $refer[$data[$pk]] = &$list[$key];
        }
        foreach ($list as $key => $data) {
            // 判断是否存在parent
            $parentId =  $data[$pid];
            if ($root == $parentId) {
                $result[] = &$list[$key];
            }else{
                if (isset($refer[$parentId])) {
                    $parent = &$refer[$parentId];
                    $parent[$child][] = &$list[$key];
                }
            }
        }
        return $result;
    }
}