<?php
/**
 * 支持ThinkPHP6.0.0+的权限验证类
 * 使用本类，请确认已下必要条件：
 * 1、php > 7.1.0
 * 2、topthink/framework > 6.0.0
 * 3、topthink/think-orm > 2.0
 */
namespace jansen\utils\auth60;

use think\facade\Db;
use think\facade\Config;
use think\facade\Session;
use think\facade\Request;

class Auth{
    /**
     * @var object 对象实例
     */
    protected static $instance;
    /**
     * 当前请求实例
     * @var Request
     */
    protected $request;

    //默认配置
    protected $config = [
        'auth_on' => 1, // 权限开关
        'auth_type' => 1, // 认证方式，1为实时认证；2为登录认证。
        'auth_roles' => 'auth_roles', // 用户组数据表名
        'auth_roles_access' => 'auth_roles_access', // 用户-用户组关系表
        'auth_rules' => 'auth_rules', // 权限规则表
        'auth_user' => 'users', // 用户信息表
    ];

    /**
     * 类架构函数
     * Auth constructor.
     */
    public function __construct(){
        //可设置配置项 auth, 此配置项为数组。
        if ($auth = Config::get('auth')) {
            $this->config = array_merge($this->config, $auth);
        }

        // 初始化request
        $this->request = Request::instance();
    }

    /**
     * 初始化
     * @param array $options
     * @return object|static
     * @author:Jansen <6206574@qq.com>
     */
    public static function instance($options = []){
        if (is_null(self::$instance)) {
            self::$instance = new static($options);
        }
        return self::$instance;
    }

    /**
     * 检查权限
     * @param string|array $name  需要验证的规则列表,支持逗号分隔的权限规则或索引数组
     * @param int $uid           认证用户的id
     * @param string $relation 如果为 'or' 表示满足任一条规则即通过验证;如果为 'and'则表示需满足所有规则才能通过验证
     * @param int $type 认证类型
     * @param string $mode 执行check的模式
     * @return bool               通过验证返回true;失败返回false
     */
    public function check($name, $uid, $relation = 'or', $type = 1, $mode = 'url'){
        if (!$this->config['auth_on']) {
            return true;
        }
        // 获取用户需要验证的所有有效规则列表
        $authList = $this->getAuthList($uid, $type);
        if (is_string($name)) {
            $name = strtolower($name);
            if (strpos($name, ',') !== false) {
                $name = explode(',', $name);
            } else {
                $name = [$name];
            }
        }
        $list = []; //保存验证通过的规则名
        if ('url' == $mode) {
            $REQUEST = unserialize(strtolower(serialize($this->request->param())));
        }
        foreach ($authList as $auth) {
            $query = preg_replace('/^.+\?/U', '', $auth);
            if ('url' == $mode && $query != $auth) {
                parse_str($query, $param); //解析规则中的param
                $intersect = array_intersect_assoc($REQUEST, $param);
                $auth = preg_replace('/\?.*$/U', '', $auth);
                if (in_array($auth, $name) && $intersect == $param) {
                    //如果节点相符且url参数满足
                    $list[] = $auth;
                }
            } else {
                if (in_array($auth, $name)) {
                    $list[] = $auth;
                }
            }
        }
        if ('or' == $relation && !empty($list)) {
            return true;
        }
        $diff = array_diff($name, $list);
        if ('and' == $relation && empty($diff)) {
            return true;
        }

        return false;
    }

    /**
     * 根据用户id获取用户组,返回值为数组
     * @param  int $uid     用户id
     * @return array       用户所属的用户组 array(
     *     array('uid'=>'用户id','group_id'=>'用户组id','title'=>'用户组名称','rules'=>'用户组拥有的规则id,多个,号隔开'),
     *     ...)
     */
    public function getRoles($uid){
        static $roles = [];
        if (isset($roles[$uid])) {
            return $roles[$uid];
        }
        //取当前用户所属的分组ID
        $userRoleIds = Db::name($this->config['auth_roles_access'])->where('uid', $uid)->column('role_id');
        //取当前用户所属分组的权限列表
        $roleInfos = Db::name($this->config['auth_roles'])->where('status','=',1)->whereIn('id', $userRoleIds)->column('id,title,rules');
        $user_roles = [];
        if (!empty($roleInfos)){
            foreach($roleInfos as $item){
                $user_roles[] = [
                    'uid'       => $uid,
                    'role_id'  => $item['id'],
                    'title'     => $item['title'],
                    'rules'     => $item['rules']
                ];
            }
        }
        $roles[$uid] = $user_roles ?: [];

        return $roles[$uid];
    }

    /**
     * 获得权限列表
     * @param int $uid 用户id
     * @param int $type
     * @return array
     */
    protected function getAuthList($uid, $type){
        static $_authList = []; //保存用户验证通过的权限列表
        if (isset($_authList[$uid . $type])) {
            return $_authList[$uid . $type];
        }
        if (2 == $this->config['auth_type'] && Session::has('_auth_list_' . $uid . $type)) {
            return Session::get('_auth_list_' . $uid . $type);
        }
        //读取默认权限
        $defaultRules = Db::name($this->config['auth_rules'])->where(['type'=>$type, 'status'=>1, 'default'=>1])->column('condition,name');
        //读取用户所属用户组
        $roles = $this->getRoles($uid);
        $ids = []; //保存用户所属用户组设置的所有权限规则id
        foreach ($roles as $g) {
            $ids = array_merge($ids, explode(',', trim($g['rules'], ',')));
        }
        $ids = array_unique($ids);
        if (count($ids) == 0) {
            $_authList[$uid . $type] = [];
            return [];
        }
        $map = [['id', 'IN', $ids], ['type', '=', $type], ['status', '=', 1]];
        //读取用户组所有权限规则
        $rules = Db::name($this->config['auth_rules'])->where($map)->column('condition,name');
        //合并角色权限和默认权限
        $rules = array_merge($defaultRules?$defaultRules:[], $rules);
        //循环规则，判断结果。
        $authList = []; //
        foreach ($rules as $rule) {
            if (!empty($rule['condition'])) {
                //根据condition进行验证
                $user = $this->getUserInfo($uid); //获取用户信息,一维数组
                $command = preg_replace('/\{(\w*?)\}/', '$user[\'\\1\']', $rule['condition']);
                //dump($command); //debug
                @(eval('$condition=(' . $command . ');'));
                if ($condition) {
                    $authList[] = strtolower($rule['name']);
                }
            } else {
                //只要存在就记录
                $authList[] = strtolower($rule['name']);
            }
        }
        $_authList[$uid . $type] = $authList;
        if (2 == $this->config['auth_type']) {
            //规则列表结果保存到session
            Session::set('_auth_list_' . $uid . $type, $authList);
        }

        return array_unique($authList);
    }

    /**
     * 获得用户资料,根据自己的情况读取数据库
     * @param $uid
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author:Jansen <6206574@qq.com>
     */
    protected function getUserInfo($uid){
        static $userinfo = [];

        $user = Db::name($this->config['auth_user']);
        // 获取用户表主键
        $_pk = is_string($user->getPk()) ? $user->getPk() : 'uid';
        if (!isset($userinfo[$uid])) {
            $userinfo[$uid] = $user->where($_pk, $uid)->find();
        }

        return $userinfo[$uid];
    }
}