<?php
// +----------------------------------------------------------------------
// | 在我们年轻的城市里，没有不可能的事！
// +----------------------------------------------------------------------
// | Copyright (c) 2020 All rights reserved.
// +----------------------------------------------------------------------
// | Author : Jansen <jansen.shi@qq.com>
// +----------------------------------------------------------------------

namespace jansen\utils\validator;

class Dns{
    /**
     * 是否DNS主机头
     * @param string $host
     * @return bool
     */
    public static function isHost(string $host){
        $host = strtolower($host);
        if (in_array($host, ['@', '*'])) return true;
        if (!preg_match('/^[a-z0-9\-\.]+$/i', $host)) return false;
        if (substr($host, 0, 1)=='-' || substr($host, -1)=='-') return false;
        if (substr($host, 0, 1)=='.' || substr($host, -1)=='.') return false;
        return true;
    }
}