<?php
// +----------------------------------------------------------------------
// | 在我们年轻的城市里，没有不可能的事！
// +----------------------------------------------------------------------
// | Copyright (c) 2020 http://utils All rights reserved.
// +----------------------------------------------------------------------
// | Author : Jansen <6206574@qq.com>
// +----------------------------------------------------------------------
namespace jansen\utils\validator;
class IP{
    /**
     * 是否IP地址
     * @param string $ip
     * @return bool
     */
    public static function is(string $ip){
        return filter_var($ip, FILTER_VALIDATE_IP) ? true : false;
    }

    /**
     * 是否IPv4格式的IP
     * @param string $ip
     * @return bool
     */
    public static function isIPv4(string $ip){
        return filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) ? true : false;
    }

    /**
     * 是否IPv6格式的IP
     * @param string $ip
     * @return bool
     */
    public static function isIPv6(string $ip){
        return filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) ? true : false;
    }
}