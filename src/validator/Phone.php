<?php
// +----------------------------------------------------------------------
// | 在我们年轻的城市里，没有不可能的事！
// +----------------------------------------------------------------------
// | Copyright (c) 2020 http://utils All rights reserved.
// +----------------------------------------------------------------------
// | Author : Jansen <6206574@qq.com>
// +----------------------------------------------------------------------
namespace jansen\utils\validator;
class Phone{
    /**
     * 验证是否固话号码
     * 支持的格式：
     * (3-4位区号)-(7-8位号码)
     * (3-4位区号)-(7-8位号码)-(1-5位分机号)
     * (3-4位区号)(7-8位号码)
     * (3-4位区号)(7-8位号码)-(1-5位分机号)
     * @param string $number
     * @return bool
     * @author:Jansen <6206574@qq.com>
     */
    public static function isPhone(string $number){
        $pattern = '/^0[1-9][\d]{1,2}\-?[1-9][\d]{6,7}(\-[\d]{1,5})?$/';
        return preg_match($pattern, $number) ? true : false;
    }
    
    /**
     * 验证是否手机号码
     * @param string $number
     * @return bool
     * @author:Jansen <6206574@qq.com>
     */
    public static function isMobile(string $number){
        $pattern = '/^1[3-9][\d]{9}$/';
        return preg_match($pattern, $number) ? true : false;
    }
    
    /**
     * 验证是否固话或手机号码
     * @param string $number
     * @return bool
     * @author:Jansen <6206574@qq.com>
     */
    public static function is(string $number){
        return self::isPhone($number) || self::isMobile($number);
    }
}