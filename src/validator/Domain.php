<?php
// +----------------------------------------------------------------------
// | 在我们年轻的城市里，没有不可能的事！
// +----------------------------------------------------------------------
// | Copyright (c) 2020 All rights reserved.
// +----------------------------------------------------------------------
// | Author : Jansen <jansen.shi@qq.com>
// +----------------------------------------------------------------------

namespace jansen\utils\validator;

use jansen\utils\domain\Domain as DomainParser;
class Domain{
    /**
     * 检测域名是否合法格式
     * @param string $domain
     * @return bool
     */
    public static function is(string $domain):bool {
        if (self::isChinese($domain)){
            return DomainParser::getDomainNameLengthForWord($domain)<=20;
        }
        if (strlen($domain) > 67)   return false;
        $name = DomainParser::getDomainName($domain);
        if (!$name) return false;
        if (!preg_match('/[a-z0-9\-]+/', $name, $match))  return false;
        if (strpos($name, '-')===0 || strrpos($name, '-')===(strlen($name)-1))    return false;
        $suffix = DomainParser::getSuffix($domain);
        if (!$suffix) return false;
        $suffix = substr($suffix, 1);
        if (!preg_match('/[a-z0-9\-]+/', $suffix, $match))  return false;
        if (strpos($suffix, '-')===0 || strrpos($suffix, '-')===(strlen($suffix)-1))    return false;
        return true;
    }

    /**
     * 是否中文域名,包括名称为中文或后缀为中文或两者都有
     * @param string $domain
     * @return bool
     */
    public static function isChinese(string $domain):bool {
        //是否中文名称
        if (!self::isChineseName($domain)){
            //是否英文名称
            $name = DomainParser::getDomainName($domain);
            if (!$name) return false;
            if (!preg_match('/[a-z0-9\-]+/', $name, $match))  return false;
            if (strpos($name, '-')===0 || strrpos($name, '-')===(strlen($name)-1))    return false;
        }
        //是否中文后缀
        if (!self::isChineseSuffix($domain)){
            //是否英文后缀
            $suffix = DomainParser::getSuffix($domain);
            if (!$suffix) return false;
            $suffix = substr($suffix, 1);
            if (!preg_match('/[a-z0-9\-]+/', $suffix, $match))  return false;
            if (strpos($suffix, '-')===0 || strrpos($suffix, '-')===(strlen($suffix)-1))    return false;
        }
        return true;
    }

    /**
     * 域名名称是否为中文
     * @param string $domain
     * @return bool
     */
    public static function isChineseName(string $domain):bool {
        if (empty($domain)) return false;
        $name = DomainParser::getDomainName($domain);
        if (!$name)   return false;
        $name = DomainParser::encode($name);
        if (!$name)   return false;
        //以xn--开头，且只包含字母、数字、-
        if (!preg_match('/[a-z0-9\-]+/', $name, $match))  return false;
        return strpos($name, 'xn--')===0 && $name==$match[0];
    }

    /**
     * 后缀是否中文
     * @param string $domain
     * @return bool
     */
    public static function isChineseSuffix(string $domain):bool {
        if (empty($domain)) return false;
        $suffix = DomainParser::getSuffix($domain);
        if (!$suffix)   return false;
        $suffix = DomainParser::encode(substr($suffix, 1));
        if (!$suffix)   return false;
        //以xn--开头，且只包含字母、数字、-
        if (!preg_match('/[a-z0-9\-]+/', $suffix, $match))  return false;
        return strpos($suffix, 'xn--')===0 && $suffix==$match[0];
    }
}