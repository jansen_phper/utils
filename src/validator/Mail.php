<?php
// +----------------------------------------------------------------------
// | 在我们年轻的城市里，没有不可能的事！
// +----------------------------------------------------------------------
// | Copyright (c) 2020 http://utils All rights reserved.
// +----------------------------------------------------------------------
// | Author : Jansen <6206574@qq.com>
// +----------------------------------------------------------------------
namespace jansen\utils\validator;
class Mail{
    /**
     * 是否电子邮件地址
     * @param string $value
     * @return bool
     * @author:Jansen <6206574@qq.com>
     */
    public static function isEmail(string $value){
        return filter_var($value, FILTER_VALIDATE_EMAIL) ? true : false;
    }

    /**
     * 是否邮政编码
     * @param string $value
     * @return bool
     * @author:Jansen <6206574@qq.com>
     */
    public static function isPostcode(string $value){
        return is_numeric($value)&&strlen($value)==6 ? true : false;
    }

    /**
     * 是否符合国际规范的通讯地址
     * 要求字符数在6个以上
     * @param string $address
     * @param string $type 检测类型 可选值 cn中文地址 en英文地址
     * @return bool
     */
    public static function isAddress(string $address, string $type='cn'){
        $pattern = ($type === 'cn')?'/^[\x{4e00}-\x{9fa5}\x{f900}-\x{fa2d}A-Za-z0-9\s\-]{0,}[\x{4e00}-\x{9fa5}\x{f900}-\x{fa2d}]+[\x{4e00}-\x{9fa5}\x{f900}-\x{fa2d}A-Za-z0-9\s\-]{0,}$/u':'/^[A-Za-z0-9\-\s\.\,]{0,}[A-Za-z]+[A-Za-z0-9\-\s\.\,]{0,}$/i';
        if (preg_match($pattern, $address)){
            if ($type != 'en'){
                if (preg_match_all('/[\x{4e00}-\x{9fa5}\x{f900}-\x{fa2d}]+/u', $address, $matchs)){
                    if (mb_strlen(implode('', $matchs[0])) >= 6)    return true;
                }
            }else {
                return true;
            }
        }
        return false;
    }
}