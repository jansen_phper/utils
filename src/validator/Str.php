<?php
// +----------------------------------------------------------------------
// | 在我们年轻的城市里，没有不可能的事！
// +----------------------------------------------------------------------
// | Copyright (c) 2020 http://utils All rights reserved.
// +----------------------------------------------------------------------
// | Author : Jansen <6206574@qq.com>
// +----------------------------------------------------------------------
namespace jansen\utils\validator;
class Str{
    /**
     * 是否全中文
     * @param string $string
     * @return bool
     */
    public static function isChinese(string $string){
        $pattern = '/^[\x{4e00}-\x{9fa5}\x{f900}-\x{fa2d}]+$/u';
        return preg_match($pattern, $string)?true:false;
    }
}