<?php
// +----------------------------------------------------------------------
// | 在我们年轻的城市里，没有不可能的事！
// +----------------------------------------------------------------------
// | Copyright (c) 2020 http://www.mysite.com All rights reserved.
// +----------------------------------------------------------------------
// | Author : Jansen <6206574@qq.com>
// +----------------------------------------------------------------------
namespace jansen\utils\ip\drivers;
/**
 * 纯真IP库解析
 * @see http://www.cz88.net
 * @package jansen\utils\ip\drivers
 */
class Qqwry{
    /**
     * @var false|resource $fp IP数据库文件句柄
     */
    private $fp;
    /**
     * @var mixed $first 第一条索引
     */
    private $first;
    /**
     * @var mixed $last 最后一条索引
     */
    private $last;
    /**
     * @var int $total 索引总数
     */
    private $total;
    /**
     * @var string $qqwry_path IP库文件路径
     */
    private $qqwry_path = __DIR__.'/qqwry.dat';
    /**
     * 内置IP库版本号
     */
    private const QQWRY_VERSION = '2020.04.20';
    /**
     * 构造函数
     * 
     * @param array $config
     */
    public function __construct(array $config=[]) {
        if (key_exists('path', $config) && file_exists($config['path'])){
            $this->qqwry_path = $config['path'];
        }
        $this->fp = fopen($this->qqwry_path, 'rb'); //qqwry.dat文件
        $this->first = $this->long4();
        $this->last = $this->long4();
        $this->total = ($this->last - $this->first) / 7; //每条索引7字节
    }
    /**
     * 检查IP合法性
     * @param string $ip IP字符串
     * @return bool
     * @author:Jansen <jansen.shi@qq.com>
     */
    private function check($ip) {
        return filter_var($ip, FILTER_VALIDATE_IP) ? true : false;
    }
    /**
     * 取4字节偏移量
     * @return int
     * @author:Jansen <jansen.shi@qq.com>
     */
    private function long4() {
        //读取little-endian编码的4个字节转化为长整型数
        $result = unpack('Vlong', fread($this->fp, 4));
        return $result['long'];
    }
    /**
     * 取3字节偏移量
     * @return int
     * @author:Jansen <jansen.shi@qq.com>
     */
    private function long3() {
        //读取little-endian编码的3个字节转化为长整型数
        $result = unpack('Vlong', fread($this->fp, 3).chr(0));
        return $result['long'];
    }
    /**
     * 取国家地区信息
     * @param string $data 二次重定向时的上一次地区信息
     * @return string
     * @author:Jansen <jansen.shi@qq.com>
     */
    private function country($data = "") {
        $char = fread($this->fp, 1);
        while (ord($char) != 0) { //国家地区信息以0结束
            $data .= $char;
            $char = fread($this->fp, 1);
        }
        return $data;
    }
    /**
     * 取所属运营商或区域或节点信息
     * @return string
     * @author:Jansen <jansen.shi@qq.com>
     */
    private function region() {
        $byte = fread($this->fp, 1); //标志字节
        switch (ord($byte)) {
            case 0:
                $area = '';
                break; //没有地区信息
            case 1: //地区被重定向
                fseek($this->fp, $this->long3());
                $area = $this->country();
                break;
            case 2: //地区被重定向
                fseek($this->fp, $this->long3());
                $area = $this->country();
                break;
            default:
                $area = $this->country($byte);
                break; //地区没有被重定向
        }
        return $area;
    }
    /**
     * 取ip对应的地址信息
     * @param string $ip ip地址
     * @return array
     * @author:Jansen <jansen.shi@qq.com>
     */
    public function ip2addr(string $ip) {
        $result = [];
        if(!$this -> check($ip))    return $result;
        $ip = pack('N', intval(ip2long($ip)));
        //二分查找
        $l = 0;
        $r = $this->total;
        while($l <= $r) {
            $m = floor(($l + $r) / 2); //计算中间索引
            fseek($this->fp, $this->first + $m * 7);
            $beginip = strrev(fread($this->fp, 4)); //中间索引的开始IP地址
            fseek($this->fp, $this->long3());
            $endip = strrev(fread($this->fp, 4)); //中间索引的结束IP地址
            if ($ip < $beginip) { //用户的IP小于中间索引的开始IP地址时
                $r = $m - 1;
            } else {
                if ($ip > $endip) { //用户的IP大于中间索引的结束IP地址时
                    $l = $m + 1;
                } else { //用户IP在中间索引的IP范围内时
                    $findip = $this->first + $m * 7;
                    break;
                }
            }
        }
        //查询国家地区信息
        fseek($this->fp, $findip);
        $result['beginip'] = long2ip($this->long4()); //用户IP所在范围的开始地址
        $offset = $this->long3();
        fseek($this->fp, $offset);
        $result['endip'] = long2ip($this->long4()); //用户IP所在范围的结束地址
        $byte = fread($this->fp, 1); //标志字节
        switch (ord($byte)) {
            case 1:  //国家和区域信息都被重定向
                $countryOffset = $this->long3(); //重定向地址
                fseek($this->fp, $countryOffset);
                $byte = fread($this->fp, 1); //标志字节
                switch (ord($byte)) {
                    case 2: //国家信息被二次重定向
                        fseek($this->fp, $this->long3());
                        $result['country'] = $this->country();
                        fseek($this->fp, $countryOffset + 4);
                        $result['region'] = $this->region();
                        break;
                    default: //国家信息没有被二次重定向
                        $result['country'] = $this->country($byte);
                        $result['region'] = $this->region();
                        break;
                }
                break;
            case 2: //国家信息被重定向
                fseek($this->fp, $this->long3());
                $result['country'] = $this->country();
                fseek($this->fp, $offset + 8);
                $result['region'] = $this->region();
                break;
            default: //国家信息没有被重定向
                $result['country'] = $this->country($byte);
                $result['region'] = $this->region();
                break;
        }
        //gb2312 to utf-8（去除无信息时显示的CZ88.NET）
        $result['country'] = str_replace('CZ88.NET','',iconv('gb2312', 'utf-8', $result['country']));
        $result['region'] = str_replace('CZ88.NET','',iconv('gb2312', 'utf-8', $result['region']));
        return $result;
    }
    /**
     * 析构函数
     */
    public function __destruct() {
        fclose($this->fp);
    }
}