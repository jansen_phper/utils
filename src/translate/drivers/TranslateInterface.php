<?php
// +----------------------------------------------------------------------
// | 在我们年轻的城市里，没有不可能的事！
// +----------------------------------------------------------------------
// | Copyright (c) 2020 http://utils All rights reserved.
// +----------------------------------------------------------------------
// | Author : Jansen <6206574@qq.com>
// +----------------------------------------------------------------------
namespace jansen\utils\translate\drivers;
interface TranslateInterface{
    public function __construct(array $config);
    /**
     * 文本翻译
     * @param string $content   待翻译文本
     * @param string $target    目标语言
     * @param string $source    源语言
     * @return string
     * @author:Jansen <6206574@qq.com>
     */
    public function text(string $content, string $target, string $source);
}