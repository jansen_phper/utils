<?php
// +----------------------------------------------------------------------
// | 在我们年轻的城市里，没有不可能的事！
// +----------------------------------------------------------------------
// | Copyright (c) 2020 http://utils All rights reserved.
// +----------------------------------------------------------------------
// | Author : Jansen <6206574@qq.com>
// +----------------------------------------------------------------------
/**
 * 百度翻译驱动
 * 支持传入的config参数如下：
 *   secretKey：string，必填，密钥
 *   projectId：string，必填，项目ID，即appId
 * @package jansen\utils\translate\drivers
 */
namespace jansen\utils\translate\drivers;
use jansen\utils\translate\exception\BaiduTranslateException;
class Baidu implements TranslateInterface{
    /**
     * @var string 项目ID，可以根据控制台-开发者信息中的配置填写
     */
    private $projectId;
    /**
     * @var string $secretKey 密钥
     */
    private $secretKey;
    /**
     * @var int $random 随机数
     */
    private $random;
    /**
     * @var string $endpoint API接口调用完整域名
     */
    private $endpoint = 'https://fanyi-api.baidu.com/api/trans/vip/translate';
    public function __construct(array $config){
        key_exists('projectId', $config) && $this->setProjectId($config['projectId']);
        key_exists('secretKey', $config) && $this->setSecretKey($config['secretKey']);
        $this->setRandom();
    }
    /**
     * 生成随机数
     * @author:Jansen <6206574@qq.com>
     */
    public function setRandom(){
        $this->random = rand(1000, 9999);
    }
    /**
     * 设置项目ID
     * @param string $id
     * @return void
     * @author:Jansen <6206574@qq.com>
     */
    public function setProjectId(string $id){
        $this->projectId = $id;
    }
    /**
     * 设置SecretKey
     * @param string $secretKey
     * @return void
     * @author:Jansen <6206574@qq.com>
     */
    public function setSecretKey(string $secretKey){
        $this->secretKey = $secretKey;
    }
    /**
     * 文本翻译
     * @param string $content   待翻译文本
     * @param string $target    目标语言
     * @param string $source    源语言
     * @return string
     * @author:Jansen <6206574@qq.com>
     */
    public function text(string $content, string $target='en', string $source='auto'){
        $query['q']     = $content;
        $query['from']  = $source;
        $query['to']    = $target;
        $query['appid'] = $this->projectId;
        $query['salt']  = $this->random;
        $query['sign']  = $this->authorization($content);
        $httpClient = new \GuzzleHttp\Client();
        $response = $httpClient->post($this->endpoint, [
            'connect_timeout'   => 5,
            'timeout'           => 5,
            'form_params'       => $query
        ]);
        $result = json_decode($response->getBody()->getContents(), true);
        if (isset($result['error_code'])){
            throw new BaiduTranslateException($result['error_msg'], $result['error_code']);
        }
        return $result['trans_result'][0]['dst'];
    }
    /**
     * 计算签名
     * @param string $content 待翻译文本
     * @return string
     * @author:Jansen <6206574@qq.com>
     */
    private function authorization(string $content){
        return md5($this->projectId.$content.$this->random.$this->secretKey);
    }
}