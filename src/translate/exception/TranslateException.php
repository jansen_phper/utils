<?php
// +----------------------------------------------------------------------
// | 在我们年轻的城市里，没有不可能的事！
// +----------------------------------------------------------------------
// | Copyright (c) 2020 http://www.mysite.com All rights reserved.
// +----------------------------------------------------------------------
// | Author : Jansen <6206574@qq.com>
// +----------------------------------------------------------------------'
declare (strict_types = 1);
namespace jansen\utils\translate\exception;
class TranslateException extends \RuntimeException{
    protected $errors;
    protected $errorMessage;
    protected $errorCode;
    public function __construct(string $message, string $code, \Throwable $previous = null){
        $this->errorMessage = $message;
        $this->errorCode = $code;
        parent::__construct($message, E_USER_ERROR, $previous);
    }
    /**
     * 取中文错误信息
     * @return string
     * @author:Jansen <6206574@qq.com>
     */
    public function getErrorMessage(){
        return isset($this->errors[$this->errorCode])?$this->errors[$this->errorCode]:$this->errorMessage;
    }
    /**
     * 取错误码
     * @return string
     * @author:Jansen <6206574@qq.com>
     */
    public function getErrorCode(){
        return $this->errorCode;
    }
    /**
     * 取原始错误信息
     * @author:Jansen <6206574@qq.com>
     */
    public function getError(){
        return [$this->errorCode => $this->errorMessage];
    }
}