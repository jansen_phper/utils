<?php
// +----------------------------------------------------------------------
// | 在我们年轻的城市里，没有不可能的事！
// +----------------------------------------------------------------------
// | Copyright (c) 2020 http://www.mysite.com All rights reserved.
// +----------------------------------------------------------------------
// | Author : Jansen <6206574@qq.com>
// +----------------------------------------------------------------------
namespace jansen\utils\translate\exception;
class TencentTranslateException extends TranslateException{
    protected $errors = [
        'UnsupportedOperation'                              => '操作不支持。',
        'ResourceInUse'                                     => '资源被占用。',
        'InternalError'                                     => '内部错误。',
        'RequestLimitExceeded'                              => '请求的次数超过了频率限制。',
        'AuthFailure.SecretIdNotFound'                      => '密钥不存在。 请在控制台检查密钥是否已被删除或者禁用，如状态正常，请检查密钥是否填写正确，注意前后不得有空格。',
        'LimitExceeded'                                     => '超过配额限制。',
        'NoSuchVersion'                                     => '接口版本不存在。',
        'ResourceNotFound'                                  => '资源不存在。',
        'AuthFailure.SignatureFailure'                      => '签名错误。 签名计算错误，请对照调用方式中的接口鉴权文档检查签名计算过程。',
        'AuthFailure.SignatureExpire'                       => '签名过期。Timestamp 和服务器时间相差不得超过五分钟，请检查本地时间是否和标准时间同步。',
        'UnsupportedRegion'                                 => '接口不支持所传地域。',
        'UnauthorizedOperation'                             => '未授权操作。',
        'InvalidParameter'                                  => '参数错误。',
        'ResourceUnavailable'                               => '资源不可用。',
        'AuthFailure.MFAFailure'                            => 'MFA 错误。',
        'AuthFailure.UnauthorizedOperation'                 => '请求未授权。请参考 CAM 文档对鉴权的说明。',
        'AuthFailure.InvalidSecretId'                       => '密钥非法（不是云 API 密钥类型）。',
        'AuthFailure.TokenFailure'                          => 'Token 错误。',
        'DryRunOperation'                                   => 'DryRun 操作，代表请求将会是成功的，只是多传了 DryRun 参数。',
        'FailedOperation'                                   => '操作失败。',
        'UnknownParameter'                                  => '未知参数错误。',
        'UnsupportedProtocol'                               => 'HTTP(S)请求协议错误，只支持 GET 和 POST 请求。',
        'InvalidParameterValue'                             => '参数取值错误。',
        'InvalidAction'                                     => '接口不存在。',
        'MissingParameter'                                  => '缺少参数错误。',
        'ResourceInsufficient'                              => '资源不足。',
        'FailedOperation.NoFreeAmount'                      => '本月免费额度已用完，如需继续使用您可以在机器翻译控制台升级为付费使用。',
        'FailedOperation.ServiceIsolate'                    => '账号因为欠费停止服务，请在腾讯云账户充值。',
        'FailedOperation.UserNotRegistered'                 => '服务未开通，请在腾讯云官网机器翻译控制台开通服务。',
        'InternalError.BackendTimeout'                      => '后台服务超时，请稍后重试。',
        'InternalError.ErrorUnknown'                        => '未知错误。',
        'InvalidParameter.DuplicatedSessionIdAndSeq'        => '重复的SessionUuid和Seq组合。',
        'InvalidParameter.SeqIntervalTooLarge'              => 'Seq之间的间隙请不要大于2000。',
        'UnauthorizedOperation.ActionNotFound'              => '请填写正确的Action字段名称。',
        'UnsupportedOperation.TextTooLong'                  => '单次请求text超过⻓长度限制，请保证单次请求⻓长度低于2000。',
        'UnsupportedOperation.UnSupportedTargetLanguage'    => '不支持的目标语言，请参照语言列表。',
        'UnsupportedOperation.UnsupportedLanguage'          => '不支持的语言，请参照语言列表。',
        'UnsupportedOperation.UnsupportedSourceLanguage'    => '不支持的源语言，请参照语言列表。'
    ];
}