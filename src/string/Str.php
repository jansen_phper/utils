<?php
// +----------------------------------------------------------------------
// | 在我们年轻的城市里，没有不可能的事！
// +----------------------------------------------------------------------
// | Copyright (c) 2021 All rights reserved.
// +----------------------------------------------------------------------
// | Author : Jansen <jansen.shi@qq.com>
// +----------------------------------------------------------------------
namespace jansen\utils\string;

class Str{
    /**
     * 生成复杂密码
     * @param int $length 生成的密码长度
     * @return string
     * @author:Jansen <jansen.shi@qq.com>
     */
    public static function password(int $length=32): string{
        $alpha = ['a','b','c','d','e','f','g','h','j','k','m','n','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','J','K','M','N','P','Q','R','S','T','U','V','W','X','Y','Z'];
        $number = ['0','1','2','3','4','5','6','7','8','9'];
        $symbol = ['@','#','$','%','*','+','=','_','-','^','~'];
        $symbolNum = rand(2, 3);
        $charNum = ($length - $symbolNum) / 2;
        //打乱符号数组
        shuffle($symbol);
        shuffle($alpha);
        shuffle($number);
        //随机取字符
        $result['symbol'] = array_slice($symbol, 0, $symbolNum);
        $result['number'] = array_slice($number, 0, $charNum);
        $result['alpha'] = array_slice($alpha, 0, $length-$charNum-$symbolNum);
        //合并
        $password = array_merge([], $result['symbol'], $result['alpha'], $result['number']);
        shuffle($password);
        return implode('', $password);
    }
}